# Heightmap Importer

Contains two scripts that allow you to convert and import GeoTIFF images into a
unity project as AssetBundles.

## Dependencies

- `ImageMagick 7`
- `Unity`

## Installing

Download the current master branch changeset,

```
$ git clone https://gitlab.com/koboldgames/heightmapimporter.git
```

then install the python package

```
$ cd heightmapimporter
$ python3 setup.py install
```

## Usage

The installer makes two scripts available. `convert_tree` is used to create
`PNG`-format images out of the `GeoTIFF` source data. 

```
$ convert_tree --help
usage: convert_tree [-h] [-v] [-n] [-s MAX_NUM_SCENARIOS] [-f]
                    [-c NO_CONVERSION] [--global-index GLOBAL_INDEX]
                    [--scenario-postfix SCENARIO_POSTFIX] [-k KEYFRAME_DELTA]
                    [-d DAMAGE_DATA]
                    source destination

positional arguments:
  source                the source directory tree
  destination           the destination directory tree

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         increase the verbosity
  -n, --dry-run         do not actually do anything
  -s MAX_NUM_SCENARIOS, --max-num-scenarios MAX_NUM_SCENARIOS
                        set the maximum number of scenarios to process (used
                        for debugging)
  -f, --force           overwrite any destionation files already present
  -c NO_CONVERSION, --no-conversion NO_CONVERSION
                        if set, only generate the normals, manifests, and
                        fallback scenarios. Supply the backup directory as
                        argument
  --global-index GLOBAL_INDEX
                        the name of the global index directory, where
                        manifests are placed
  --scenario-postfix SCENARIO_POSTFIX
                        allows to specify a postfix that is added to the
                        scenario name on conversion
  -k KEYFRAME_DELTA, --keyframe-delta KEYFRAME_DELTA
                        determines the number of seconds between keyframes per
                        scenario
  -d DAMAGE_DATA, --damage-data DAMAGE_DATA
                        the path to the damage data JSON file
```

`build_tree` is used to import those `PNG` images into Unity to build
`AssetBundle`s.

```
$ build_tree --help
usage: build_tree [-h] [-v] [-n] [-s MAX_NUM_SCENARIOS] [-f] [-i] [-l LOG_DIR]
                  [-b BUNDLE_SEARCH_PATTERN] [-a ASSET_SEARCH_PATTERN]
                  [-t TARGET]
                  source destination project

positional arguments:
  source                the source directory tree
  destination           the destination directory
  project               the path to the Unity project directory

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         increase the verbosity
  -n, --dry-run         do not actually do anything
  -s MAX_NUM_SCENARIOS, --max-num-scenarios MAX_NUM_SCENARIOS
                        set the maximum number of scenarios to process (used
                        for debugging)
  -f, --force           force rebuilding of all asset bundles
  -i, --build-internal-bundles
                        enable the building of project internal AssetBundles
  -l LOG_DIR, --log-dir LOG_DIR
                        the log output directory
  -b BUNDLE_SEARCH_PATTERN, --bundle-search-pattern BUNDLE_SEARCH_PATTERN
                        use this pattern to select only a subset of
                        subdirectories from the tree
  -a ASSET_SEARCH_PATTERN, --asset-search-pattern ASSET_SEARCH_PATTERN
                        use this pattern to select only a subset of assets
                        from each subdirectory in the tree
  -t TARGET, --target TARGET
                        the target platform
```


#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import setuptools
import versioneer

with open('README.md', 'r') as fh:
    long_description = fh.read()

setuptools.setup(
    name='heightmapimporter',
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    author='Eleanore Young <eleanore.young@koboldgames.ch>',
    description='Imports RAMMS height-map data into a Unity project',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/koboldgames/heightmapimporter',
    package_dir={"": "src"},
    packages=[
        'heightmapimporter',
    ],
    entry_points={
        'console_scripts': [
            'convert_tree = heightmapimporter.convert_tree:main',
            'build_tree = heightmapimporter.build_tree:main',
        ],
    },
    install_requires=[
        'tqdm >= 4.28.0',
        'jsonpickle >= 0.9.0',
    ],
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: Unix',
    ],
    python_requires='>=3.6',
)


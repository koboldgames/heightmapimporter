#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Builds Unity AssetBundles from a directory tree of files'''


import asyncio
from argparse import ArgumentParser
from pathlib import Path
from . import __version__
from .utilities import valid_directory, non_empty_directory
from .processing import build_bundles


def main() -> None:
    parser = ArgumentParser()
    parser.add_argument(
        '-V',
        '--version',
        action='version',
        version=__version__,
    )
    parser.add_argument(
        '-v',
        '--verbose',
        action='count',
        default=0,
        help='increase the verbosity',
    )
    parser.add_argument(
        "-n",
        "--dry-run",
        action="store_true",
        help="do not actually do anything",
    )
    parser.add_argument(
        "-s",
        "--max-num-scenarios",
        type=int,
        default=-1,
        help="set the maximum number of scenarios to process (used for debugging)",
    )
    parser.add_argument(
        "-f",
        "--force",
        action="store_true",
        help="force rebuilding of all asset bundles",
    )
    parser.add_argument(
        "-i",
        "--build-internal-bundles",
        action="store_true",
        help="enable the building of project internal AssetBundles",
    )
    parser.add_argument(
        "-l",
        "--log-dir",
        type=valid_directory,
        default=".",
        help="the log output directory",
    )
    parser.add_argument(
        "-b",
        "--bundle-search-pattern",
        default="h????_b??_d????_*",
        help="use this pattern to select only a subset of subdirectories from the tree",
    )
    parser.add_argument(
        "-a",
        "--asset-search-pattern",
        default="*",
        help="use this pattern to select only a subset of assets from each subdirectory in the tree",
    )
    parser.add_argument(
        "-t",
        "--target",
        default="WebGL",
        help="the target platform",
    )
    parser.add_argument(
        "source",
        type=non_empty_directory,
        help="the source directory tree",
    )
    parser.add_argument(
        "destination",
        type=Path,
        help="the destination directory",
    )
    parser.add_argument(
        "project",
        type=non_empty_directory,
        help="the path to the Unity project directory",
    )
    args = parser.parse_args()

    verbose = args.verbose
    dry_run = args.dry_run
    target = args.target
    max_iterations = args.max_num_scenarios
    force = args.force
    build_internal_bundles = args.build_internal_bundles
    bundle_search_pattern = args.bundle_search_pattern
    asset_search_pattern = args.asset_search_pattern

    source_tree = args.source.resolve()
    destination = args.destination.resolve()
    project_path = args.project.resolve()
    log_dir = args.log_dir.resolve()

    if verbose > 0 and dry_run:
        print("Dry-run mode enabled. I will not do anything")

    event_loop = asyncio.get_event_loop()
    event_loop.run_until_complete(build_bundles(
        target, source_tree, destination, project_path, bundle_search_pattern, asset_search_pattern, force, build_internal_bundles, max_iterations, log_dir, verbose, dry_run
    ))
    event_loop.close()


if __name__ == '__main__':
    main()

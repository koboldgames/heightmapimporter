# -*- coding: utf-8 -*-

from enum import Enum


class MurgangSizeDe(Enum):
    Klein = 1
    Mittel = 2
    Gross = 3

    @classmethod
    def from_str(cls, value: str) -> "MurgangSizeDe":
        for name, member in cls.__members__.items():
            if value.lower() == name.lower():
                return member

        raise Exception("Unknown enum variant {}".format(value))

    def to_str(self) -> str:
        return self.name.lower()

    def __getstate__(self) -> str:
        return self.name

    def __setstate__(self, state: str) -> None:
        deser = MurgangSizeDe.from_str(state)
        self = deser


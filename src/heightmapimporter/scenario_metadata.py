# -*- coding: utf-8 -*-

from typing import List, Optional, Iterator, Tuple
from pathlib import Path
import itertools
from .dataset_manifest import DatasetManifest
from .image_bounds import ImageBounds
from .damage_data import DamageData
from .murgang_size_de import MurgangSizeDe


KEYFRAME_DELTA = 2


class ScenarioMetadata(object):
    def __init__(self, source_scenario_dir: Path, dest_dir: Path, global_index_dir: Path, keyframe_delta: int, postfix: Optional[str] = None) -> None:
        orig_name = source_scenario_dir.name
        parts = orig_name.split("_")
        if len(parts) >= 4:
            self.__houses = [bool(int(f)) for f in parts[0][1:]]
            self.__bridges = [bool(int(f)) for f in parts[1][1:]]
            self.__dams = [bool(int(f)) for f in parts[2][1:]]
            self.__size = MurgangSizeDe.from_str(parts[3])
        else:
            self.__houses = []
            self.__bridges = []
            self.__dams = []
            self.__size = MurgangSizeDe.Gross

        self.__orig_name = orig_name
        if postfix is not None:
            self.__name = "{}_{}".format(orig_name, postfix)
        else:
            self.__name = orig_name

        self.__postfix = postfix
        if len(parts) >= 5:
            self.__postfix = parts[4]

        self.__keyframe_delta = keyframe_delta
        self.__source_dir = source_scenario_dir
        self.__dest_dir = dest_dir.joinpath(self.__name)
        self.__global_index_dir = global_index_dir
        self.__manifest_name = "{}.json".format(self.__name)
        self.__tiff_pattern = "*.tif"
        self.__png_pattern = "*.png"
        self.__flowheight_tiff_pattern = "{}_Flowheight_*.00.tif".format(orig_name)
        self.__max_flowheight_tiff_name = "{}_MaxFlowheight.tif".format(orig_name)
        self.__max_pressure_tiff_name = "{}_MaxPressure.tif".format(orig_name)
        self.__max_velocity_tiff_name = "{}_MaxVelocity.tif".format(orig_name)
        self.__flowheight_png_pattern = "{}_Flowheight_*.00.png".format(orig_name)
        self.__normals_png_pattern = "{}_Normals_*.00.png".format(orig_name)
        self.__max_flowheight_name = "{}_MaxFlowheight.png".format(orig_name)
        self.__max_pressure_name = "{}_MaxPressure.png".format(orig_name)
        self.__max_velocity_name = "{}_MaxVelocity.png".format(orig_name)

        self.__is_fallback = "h0000_b01_d0000" in orig_name
        self.__fallback_name = "fallback_{}".format(self.__size.to_str())
        self.__dest_fallback_dir = dest_dir.joinpath(self.__fallback_name)
        self.__fb_max_flowheight_name = "{}_MaxFlowheight.png".format(self.__fallback_name)
        self.__fb_max_pressure_name = "{}_MaxPressure.png".format(self.__fallback_name)
        self.__fb_max_velocity_name = "{}_MaxVelocity.png".format(self.__fallback_name)
        self.__fb_flowheight_png_pattern = "{}_Flowheight_*.00.png".format(self.__fallback_name)
        self.__fb_normals_png_pattern = "{}_Normals_*.00.png".format(self.__fallback_name)


    @property
    def name(self) -> str:
        return self.__name

    @property
    def is_fallback(self) -> bool:
        return self.__is_fallback

    @property
    def keyframe_delta(self) -> int:
        return self.__keyframe_delta

    @property
    def manifest_name(self) -> str:
        return self.__manifest_name

    @property
    def dest_dir(self) -> Path:
        return self.__dest_dir

    @property
    def dest_fallback_dir(self) -> Path:
        return self.__dest_fallback_dir

    @property
    def dest_max_flowheight_file(self) -> Path:
        return self.__dest_dir.joinpath(self.__max_flowheight_name)

    @property
    def dest_max_pressure_file(self) -> Path:
        return self.__dest_dir.joinpath(self.__max_pressure_name)

    @property
    def dest_max_velocity_file(self) -> Path:
        return self.__dest_dir.joinpath(self.__max_velocity_name)

    @property
    def dest_fb_max_flowheight_file(self) -> Path:
        return self.__dest_fallback_dir.joinpath(self.__fb_max_flowheight_name)

    @property
    def dest_fb_max_pressure_file(self) -> Path:
        return self.__dest_fallback_dir.joinpath(self.__fb_max_pressure_name)

    @property
    def dest_fb_max_velocity_file(self) -> Path:
        return self.__dest_fallback_dir.joinpath(self.__fb_max_velocity_name)

    @property
    def dest_manifest_path(self) -> Path:
        return self.__global_index_dir.joinpath(self.__manifest_name)

    @property
    def dest_fallback_manifest_path(self) -> Path:
        return self.__dest_fallback_dir.joinpath("manifest.json")

    def find_source_tiffs(self) -> Iterator[Path]:
        # Find the special files, those that aren't keyframes
        st_specials_pre1 = [
            self.__max_flowheight_tiff_name,
            self.__max_pressure_tiff_name,
            self.__max_velocity_tiff_name
        ]
        st_specials = (self.__source_dir.joinpath(p) for p in st_specials_pre1)

        # Chain the keyframe files and the special files
        return itertools.chain(
            self.find_source_flowheight_tiffs(),
            st_specials,
        )

    def find_fb_pngs(self) -> Iterator[Tuple[Path, Path]]:
        return ((s, self._as_fb_png_path(s)) for s in self.__dest_dir.glob(self.__png_pattern))

    def find_source_flowheight_tiffs(self) -> Iterator[Path]:
        # Find all keyframe tiff files
        sft_pre1 = self.__source_dir.glob(self.__flowheight_tiff_pattern)

        # Sort the keyframes
        sft_pre2 = sorted(sft_pre1)

        # Skip every n-th keyframe, where n is the integer multiple of KEYFRAME_DELTA
        stride = self.__keyframe_delta // KEYFRAME_DELTA

        return (p for (i, p) in enumerate(sft_pre2) if i % stride == 0)

    def find_source_max_flowheight_tiffs(self) -> Iterator[Path]:
        return self.__source_dir.glob(self.__max_flowheight_tiff_name)

    def find_dest_flowheight_pngs(self) -> Iterator[Path]:
        return self.__dest_dir.glob(self.__flowheight_png_pattern)

    def find_dest_normals_pngs(self) -> Iterator[Path]:
        return self.__dest_dir.glob(self.__normals_png_pattern)

    def find_dest_fb_flowheight_pngs(self) -> Iterator[Path]:
        return self.__dest_fallback_dir.glob(self.__fb_flowheight_png_pattern)

    def find_dest_fb_normals_pngs(self) -> Iterator[Path]:
        return self.__dest_fallback_dir.glob(self.__fb_normals_png_pattern)

    def build_manifest(self, murgang_bounds: ImageBounds, max_flowheight_bounds: ImageBounds, damage_data: Optional[DamageData]) -> DatasetManifest:
        return DatasetManifest(
            self.__name,
            self.__houses,
            self.__bridges,
            self.__dams,
            self.__size,
            1.0 / self.__keyframe_delta,
            murgang_bounds,
            max_flowheight_bounds,
            damage_data,
            self.dest_max_flowheight_file,
            self.dest_max_pressure_file,
            self.dest_max_velocity_file,
            self.find_dest_flowheight_pngs(),
            self.find_dest_normals_pngs(),
        )

    def build_fallback_manifest(self, murgang_bounds: ImageBounds, max_flowheight_bounds: ImageBounds, damage_data: Optional[DamageData]) -> DatasetManifest:
        return DatasetManifest(
            self.__fallback_name,
            self.__houses,
            self.__bridges,
            self.__dams,
            self.__size,
            1.0 / self.__keyframe_delta,
            murgang_bounds,
            max_flowheight_bounds,
            damage_data,
            self.dest_fb_max_flowheight_file,
            self.dest_fb_max_pressure_file,
            self.dest_fb_max_velocity_file,
            self.find_dest_fb_flowheight_pngs(),
            self.find_dest_fb_normals_pngs(),
        )

    def _as_fb_png_path(self, source_png: Path) -> Path:
        new_name = source_png.name.replace(self.__name, self.__fallback_name)
        return self.__dest_fallback_dir.joinpath(new_name)


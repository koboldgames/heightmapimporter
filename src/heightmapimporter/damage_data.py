# -*- coding: utf-8 -*-

class DamageData(object):
    def __init__(self, person_damage: float, property_damage: float, supply_damage: float) -> None:
        self.person_damage = person_damage
        self.property_damage = property_damage
        self.supply_damage = supply_damage

    def __repr__(self) -> str:
        return "DamageData(person_damage={}, property_damage={}, supply_damage={})".format(self.person_damage, self.property_damage, self.supply_damage)


#/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Converts a directory tree of TIFF files to a tree of PNG files'''


import asyncio
from argparse import ArgumentParser
from pathlib import Path
from . import __version__
from .utilities import non_empty_directory, optional_valid_file, image_delta, optional_nonex_path
from .processing import convert_scenarios
from .scenario_metadata import KEYFRAME_DELTA


def main() -> None:
    parser = ArgumentParser()
    parser.add_argument(
        '-V',
        '--version',
        action='version',
        version=__version__,
    )
    parser.add_argument(
        '-v',
        '--verbose',
        action='count',
        default=0,
        help='increase the verbosity',
    )
    parser.add_argument(
        "-n",
        "--dry-run",
        action="store_true",
        help="do not actually do anything",
    )
    parser.add_argument(
        "-s",
        "--max-num-scenarios",
        type=int,
        default=-1,
        help="set the maximum number of scenarios to process (used for debugging)",
    )
    parser.add_argument(
        "-f",
        "--force",
        action="store_true",
        help="overwrite any destionation files already present",
    )
    parser.add_argument(
        "-c",
        "--no-conversion",
        type=optional_nonex_path,
        default=None,
        help="if set, only generate the normals, manifests, and fallback scenarios. Supply the backup directory as argument",
    )
    parser.add_argument(
        "--global-index",
        type=Path,
        default=Path("global/manifests"),
        help="the name of the global index directory, where manifests are placed",
    )
    parser.add_argument(
        "--scenario-postfix",
        default=None,
        help="allows to specify a postfix that is added to the scenario name on conversion",
    )
    parser.add_argument(
        "-k",
        "--keyframe-delta",
        type=image_delta,
        default=KEYFRAME_DELTA,
        help="determines the number of seconds between keyframes per scenario",
    )
    parser.add_argument(
        "-d",
        "--damage-data",
        type=optional_valid_file,
        default=None,
        help="the path to the damage data JSON file",
    )
    parser.add_argument(
        'source',
        type=non_empty_directory,
        help='the source directory tree',
    )
    parser.add_argument(
        'destination',
        type=Path,
        help='the destination directory tree',
    )

    args = parser.parse_args()
    force = args.force
    verbose = args.verbose
    dry_run = args.dry_run
    no_conversion = args.no_conversion
    max_iterations = args.max_num_scenarios
    global_index = args.global_index
    scenario_postfix = args.scenario_postfix
    keyframe_delta = args.keyframe_delta

    ddf = args.damage_data.resolve() if args.damage_data is not None else None
    source_dir = args.source.resolve()
    dest_dir = args.destination.resolve()

    if verbose > 0 and dry_run:
        print("Dry-run mode enabled. I will not do anything")

    event_loop = asyncio.get_event_loop()
    event_loop.run_until_complete(convert_scenarios(source_dir, dest_dir, global_index, max_iterations, scenario_postfix, keyframe_delta, ddf, force, no_conversion, verbose, dry_run))
    event_loop.close()


if __name__ == '__main__':
    main()

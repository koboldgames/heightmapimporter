# -*- coding: utf-8 -*-

from typing import Optional, Iterable, Tuple, TypeVar
from pathlib import Path
from argparse import ArgumentParser, ArgumentTypeError
from .scenario_metadata import KEYFRAME_DELTA

T = TypeVar("T")

def grouped(iterable: Iterable[T], n: int) -> Iterable[Tuple[T, ...]]:
    """s -> (s0,s1,s2,...sn-1), (sn,sn+1,sn+2,...s2n-1), ..."""
    return zip(*[iter(iterable)] * n)

def image_delta(argument: str) -> int:
    """Parse the input argument as an image delta integer. Must be positive and multiples of KEYFRAME_DELTA."""

    value = KEYFRAME_DELTA
    try:
        value = int(argument)
    except ValueError:
        raise ArgumentTypeError("{} cannot be parsed as an integer value".format(argument))

    if value <= 0:
        raise ArgumentTypeError("{} is not positive".format(value))

    if (value % KEYFRAME_DELTA) != 0:
        raise ArgumentTypeError("{} is not a multiple of {}".format(value, KEYFRAME_DELTA))

    return value


def optional_nonex_path(argument: Optional[str]) -> Optional[Path]:
    """Parse the input argument as Path that does not exist"""

    if argument is None:
        return None

    value = Path(argument)
    #try:
    #    value = value.resolve(False)
    #except FileNotFoundError:
    #    pass

    if value.exists():
        raise ArgumentTypeError("{} exists already".format(value))

    return value


def optional_valid_file(argument: Optional[str]) -> Optional[Path]:
    """Parse the input argument as Path that points to a file"""

    if argument is None:
        return None

    value = Path(argument)
    try:
        value = value.resolve()
    except FileNotFoundError:
        pass

    if not value.is_file():
        raise ArgumentTypeError('{} is not a file'.format(argument))

    return value


def valid_file(argument: str) -> Path:
    """Parse the input argument as Path that points to a file"""

    value = Path(argument)
    try:
        value = value.resolve()
    except FileNotFoundError:
        pass

    if not value.is_file():
        raise ArgumentTypeError('{} is not a file'.format(argument))

    return value


def valid_directory(argument: str) -> Path:
    """Parse the input argument as Path that points to a directory"""

    value = Path(argument)
    try:
        value = value.resolve()
    except FileNotFoundError:
        pass

    if not value.is_dir():
        raise ArgumentTypeError('{} is not a directory'.format(argument))

    return value


def non_empty_directory(argument: str) -> Path:
    """Parse the input argument as Path that points to a directory with contents"""

    value = valid_directory(argument)

    if sum(1 for _ in value.iterdir()) == 0:
        raise ArgumentTypeError('{} is an empty directory'.format(argument))

    return value


def empty_directory(argument: str) -> Path:
    """Parse the input argument as Path that points to an empty directory"""

    value = valid_directory(argument)

    if sum(1 for _ in value.iterdir()) != 0:
        raise ArgumentTypeError('{} is not an empty directory'.format(argument))

    return value


# -*- coding: utf-8 -*-

import asyncio
import json
import jsonpickle
import math
import re
import struct
import shutil
import subprocess
import tqdm
from typing import Iterator, Optional
from enum import Enum
from pathlib import Path
from .utilities import non_empty_directory
from .image_bounds import ImageBounds
from .dataset_manifest import DatasetManifest
from .damage_data import DamageData
from .scenario_metadata import ScenarioMetadata, KEYFRAME_DELTA


STATISTICS_RE = re.compile(br'Channel\sstatistics:\n\s*Pixels:\s(?P<pixels>\d+)\n\s*Gray:\n\s*min:\s([-+.e0-9]+)\s+\((?P<min>[-+.e0-9]+)\)\n\s*max:\s([-+.e0-9]+)\s+\((?P<max>[-+.e0-9]+)\)', flags=re.MULTILINE);
IMAGEMAGICK = 'magick'
IM_IDENTIFY = "identify"
IM_CONVERT = "convert"
UNITY = "/Applications/Unity/Hub/Editor/2019.1.14f1/Unity.app/Contents/MacOS/Unity"
#UNITY = "unity-2019.1.14f1"


async def _extract_image_bounds(source_files: Iterator[Path], verbose: int, dry_run: bool) -> ImageBounds:
    '''Extract the minimum and maximum image values for a given scenario. The
    GeoTIFF images are scaled to the actual height values in meters (as 32-bit
    floating point numbers), and we can use those values to scale our terrain
    model.'''

    args = [
        IM_IDENTIFY,
        '-define', 'tiff:ignore-tags=33550,33922,42112,42113',
        '-verbose',
    ]
    args.extend((str(s) for s in source_files))

    if dry_run:
        if verbose > 1:
            print("Skipping the calculation of the correct image bounds")
        return ImageBounds(0.0, 1.0)

    if verbose > 1:
        print("Extracting the image bounds")
    if verbose > 2:
        print("$ {} {}".format(IMAGEMAGICK, " ".join(args)))
    p = await asyncio.create_subprocess_exec(IMAGEMAGICK, *args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (stdout, stderr) = await p.communicate()

    # Filter the program output to extract the image bounds
    min_accum = math.inf
    max_accum = -math.inf
    num_matches = 0
    for match in STATISTICS_RE.finditer(stdout):
        if match is None:
            continue

        min_value = float(match.group('min'))
        max_value = float(match.group('max'))
        min_accum = min(min_accum, min_value)
        max_accum = max(max_accum, max_value)
        num_matches += 1

    if verbose > 1:
        print("Calculated image bounds of min: {} and max: {}".format(min_accum, max_accum))

    return ImageBounds(min_accum, max_accum)


async def _convert_tiff_to_png(source_files: Iterator[Path], destination_dir: Path, verbose: int, dry_run: bool) -> None:
    '''Convert TIFF source files to PNG at the destination'''

    #args = [
    #    'mogrify',
    #    '-define', 'tiff:ignore-tags=33550,33922,42112,42113',
    #    '-define', 'colorspace:auto-grayscale=off',
    #    #'-auto-level',
    #    '-format', 'png64',
    #    '-type', 'TrueColor',
    #    '-path', str(destination_dir),
    #]
    #args.extend(("{}".format(s) for s in source_files))

    #if not dry_run:
    #    if verbose > 1:
    #        print("Converting TIFF data to PNG")
    #    if verbose > 2:
    #        print("Command: {} {}".format(IMAGEMAGICK, " ".join(args)))
    #    p = await asyncio.create_subprocess_exec(IMAGEMAGICK, *args)
    #    await p.wait()

    for source_file in source_files:
        dest_file = destination_dir.joinpath(source_file.name.replace(".tif", ".png"))
        args = [
            IM_CONVERT,
            '-define', 'tiff:ignore-tags=33550,33922,42112,42113',
            str(source_file),
            '-define', 'png:bit-depth=16',
            '-define', 'png:color-type=4',
            '-define', 'png:compression-filter=0',
            '-define', 'png:compression-level=0',
            '-background', 'black',
            str(dest_file),
        ]

        if not dry_run:
            if verbose > 1:
                print("Converting TIFF data to PNG")
            if verbose > 2:
                print("$ {} {}".format(IMAGEMAGICK, " ".join(args)))
            p = await asyncio.create_subprocess_exec(IMAGEMAGICK, *args)
            await p.wait()


def _cleanup_flowheight_pngs(scenario: ScenarioMetadata, backup_path: Path, verbose: int, dry_run: bool) -> None:
    """
    Ensure only the right amount of PNG keyframes is at the
    destination. Move all other files to the backup directory
    """
    # Calculate the stride which indicates how many files we need to skip (and move to backup)
    stride = scenario.keyframe_delta // KEYFRAME_DELTA

    # Find all keyframe TIFFs
    dt_pre1 = list(scenario.find_source_flowheight_tiffs())

    # Find all keyframe PNGs
    dp_pre1 = list(scenario.find_dest_flowheight_pngs())

    if len(dp_pre1) <= len(dt_pre1) // stride:
        if verbose > 1:
            print("The png files were already reduced in frequency")
        return

    # Sort the keyframe PNGs
    dp_pre2 = sorted(dp_pre1)

    # Determine the superfluous files
    dp_superfluous = [p for (i, p) in enumerate(dp_pre2) if i % stride != 0]

    if len(dp_superfluous) == 0:
        if verbose > 1:
            print("There are no superfluous keyframes")
        return

    #  Create the no-conversion backup directory
    if not backup_path.exists():
        if not dry_run:
            if verbose > 1:
                print("The no-conversion backup directory does not exist yet")
            backup_path.mkdir(parents=True)
    else:
        raise Exception("The no-conversion backup path exists already")

    # Move all superfluous files
    for dps in dp_superfluous:
        bps = backup_path.joinpath(dps.name)
        if not dry_run:
            if verbose > 2:
                print("Moving the file {} to backup at {}".format(dps, bps))
            shutil.move(str(dps), str(bps))


def _create_normals_png(scenario: ScenarioMetadata, verbose: int, dry_run: bool) -> None:
    '''
    Copy the flowheight files and rename them so as to create the normal maps
    (which are then converted properly on import into unity).
    '''

    dest_normals_pngs_ex = list(scenario.find_dest_normals_pngs())
    if verbose > 1:
        print("Removing {} normals files that were already present beforehand".format(len(dest_normals_pngs_ex)))
    for dnpe in dest_normals_pngs_ex:
        dnpe.unlink()

    if verbose > 1:
        print("Creating the normal map PNGs")
    for dest_flowheight_png in scenario.find_dest_flowheight_pngs():
        dest_normals_png = scenario.dest_dir.joinpath(dest_flowheight_png.name.replace('Flowheight', 'Normals'))
        if not dry_run:
            shutil.copyfile(str(dest_flowheight_png), str(dest_normals_png))


async def _generate_manifest(scenario: ScenarioMetadata, murgang_bounds: ImageBounds, max_flowheight_bounds: ImageBounds, damage_data: Optional[DamageData], verbose: int, dry_run: bool) -> None:
    '''Generate a manifest for the given scenario'''

    if verbose > 1:
        print("Generating the scenario manifest")

    # Create the manifest
    manifest = scenario.build_manifest(murgang_bounds, max_flowheight_bounds, damage_data)

    # Save the manifest
    dest_manifest_file = scenario.dest_manifest_path
    if not dry_run:
        with dest_manifest_file.open('w') as f:
            f.write(jsonpickle.encode(manifest, unpicklable=False))


async def _create_fallback(scenario: ScenarioMetadata, murgang_bounds: ImageBounds, max_flowheight_bounds: ImageBounds, damage_data: Optional[DamageData], verbose: int, dry_run: bool) -> None:
    '''
    Create a fallback scenario. This type of scenario will be built into the
    unity project and will not reside in an asset bundle
    '''
    if not scenario.is_fallback:
        return

    if verbose > 1:
        print("Creating a fallback scenario")

    # Create the fallback scenario directory
    dest_fb_dir = scenario.dest_fallback_dir
    if not dest_fb_dir.exists():
        if not dry_run:
            if verbose > 1:
                print("The fallback directory does not exist yet")
            dest_fb_dir.mkdir(parents=True)
    else:
        if verbose > 1:
            print("The destination directory exists and is not empty. Removing its contents")
        if not dry_run:
            shutil.rmtree(str(dest_fb_dir))
            dest_fb_dir.mkdir()

    # Copy all PNG files at the destination to the fallback directory
    fb_pngs = list(scenario.find_fb_pngs());
    if verbose > 1:
        print("Copying {} files fo the fallback directory".format(len(fb_pngs)))
    for (source_png, dest_png) in scenario.find_fb_pngs():
        if not dry_run:
            shutil.copyfile(str(source_png), str(dest_png))

    # Generate the fallback manifest
    dest_fb_manifest = scenario.dest_fallback_manifest_path
    fb_manifest = scenario.build_fallback_manifest(murgang_bounds, max_flowheight_bounds, damage_data)
    if not dry_run:
        if verbose > 1:
            print("Generating the fallback manifest")
        with dest_fb_manifest.open("w") as f:
            f.write(jsonpickle.encode(fb_manifest, unpicklable=False))


async def _build_bundle(target: str, source_dir: Path, dest_dir: Path, project_path: Path, asset_pat: str, log_file: Path, verbose: int, dry_run: bool) -> None:
    args = [
        "-batchmode",
        "-quit",
        "-logFile", "-",
        "-projectPath", str(project_path),
        "-executeMethod", "Koboldgames.ContinuousIntegration.AssetBundleBuilder.Invoke",
        "--",
        "-searchPattern", asset_pat,
        "-outputPath", str(dest_dir),
        "-buildDir", str(source_dir),
        target,
    ]

    if verbose > 1:
        print("Building the bundle with source data from {} to destination {}".format(source_dir, dest_dir))

    if not dry_run:
        with log_file.open("w") as f:
            p = await asyncio.create_subprocess_exec(UNITY, *args, stdout=f, stderr=subprocess.STDOUT)
            await p.wait()


async def _build_internal_bundles(target: str, project_path: Path, log_file: Path, verbose: int, dry_run: bool) -> None:
    args = [
        "-batchmode",
        "-quit",
        "-logFile", "-",
        "-projectPath", str(project_path),
        "-executeMethod", "Koboldgames.ContinuousIntegration.AssetBundleBuilder.Invoke",
        "--",
        "-buildInternalBundles",
        target,
    ]

    if not dry_run:
        with log_file.open("w") as f:
            p = await asyncio.create_subprocess_exec(UNITY, *args, stdout=f, stderr=subprocess.STDOUT)
            await p.wait()


async def build_bundles(target: str, source_tree: Path, dest_dir: Path, project_path: Path, bundle_pat: str, asset_pat: str, force: bool, build_int_bndl: bool, max_iterations: int, log_dir: Path, verbose: int, dry_run: bool) -> None:
    # Create the root destination directory if it does not exist, or delete
    # everything if the force flag is set
    if not dest_dir.exists():
        if verbose > 0:
            print("The destination directory does not exist yet")
        if not dry_run:
            dest_dir.mkdir()
    elif force and sum(1 for _ in dest_dir.iterdir()) != 0:
        if verbose > 0:
            print("The destination directory exists and is not empty. Removing its contents")
        if not dry_run:
            shutil.rmtree(str(dest_dir))
            dest_dir.mkdir()

    # Determine which bundles have already been built
    complete_bundles = frozenset(f.name for f in dest_dir.iterdir() if f.is_file() and len(f.suffixes) == 0)
    if verbose > 0:
        print("{} bundles have already been built".format(len(complete_bundles)))

    # Gather the list of source directories of bundles that still need to be built
    ssd_pre1 = (d for d in source_tree.glob(bundle_pat) if d.is_dir())
    ssd_pre2 = (d for d in ssd_pre1 if d.name not in complete_bundles)
    source_scenario_dirs = list(ssd_pre2)
    if verbose > 0:
        print("{} bundles have yet to be built".format(len(source_scenario_dirs)))

    # Abort early if the number of iterations is limited
    if max_iterations > 0 and max_iterations <= len(source_scenario_dirs):
        if verbose > 0:
            print("Will abort early after {} scenarios".format(max_iterations))
        source_scenario_dirs = source_scenario_dirs[:max_iterations]

    # Build the external bundles
    if verbose > 0:
        print("Starting the asset bundle build process")
    for source_scenario_dir in tqdm.tqdm(source_scenario_dirs):
        log_file = log_dir.joinpath("{}.log".format(source_scenario_dir.name))
        await _build_bundle(target, source_scenario_dir, dest_dir, project_path, asset_pat, log_file, verbose, dry_run)

    # Build the internal bundles, i.e. those that are seated within the asset
    # tree of the unity project.
    if build_int_bndl:
        if verbose > 0:
            print("Building the project-internal bundles")
        log_file = log_dir.joinpath("internal-bundles.log")
        await _build_internal_bundles(target, project_path, log_file, verbose, dry_run)


async def convert_scenarios(source_dir: Path, dest_dir: Path, global_index: Path, max_iterations: int, scenario_postfix: Optional[str], keyframe_delta: int, damage_data_file: Optional[Path], force: bool, no_conversion: Optional[Path], verbose: int, dry_run: bool) -> None:
    '''Convert the TIFF files at source to PNG-based scenarios'''

    # Load the damage data file
    ddl = None
    if damage_data_file is not None:
        with damage_data_file.open("r") as f:
            ddl = json.load(f)

    # Create the root destination directory if it does not exist, or delete
    # everything if the force flag is set
    if not dest_dir.exists():
        if verbose > 0:
            print("The destination directory does not exist yet")
        if not dry_run:
            dest_dir.mkdir()
    elif force and sum(1 for _ in dest_dir.iterdir()) != 0:
        if verbose > 0:
            print("The destination directory exists and is not empty. Removing its contents")
        if not dry_run:
            shutil.rmtree(str(dest_dir))
            dest_dir.mkdir()

    # Create the global index directory
    global_index_dir = dest_dir.joinpath(global_index)
    if not global_index_dir.exists():
        if verbose > 0:
            print("The global index directory does not exist yet")
        if not dry_run:
            global_index_dir.mkdir(parents=True)

    # Gather the list of source directories
    source_scenario_dirs = [d for d in source_dir.iterdir() if d.is_dir()]
    if verbose > 0:
        print("{} scenarios have yet to be converted".format(len(source_scenario_dirs)))

    # Abort early if the number of iterations is limited
    if max_iterations > 0 and max_iterations <= len(source_scenario_dirs):
        if verbose > 0:
            print("Will abort early after {} scenarios".format(max_iterations))
        source_scenario_dirs = source_scenario_dirs[:max_iterations]

    # Convert the scenarios
    if verbose > 0:
        print("Starting the scenario conversion process")
    for source_scenario_dir in tqdm.tqdm(source_scenario_dirs):
        # Obtain scenario metadata
        scenario = ScenarioMetadata(source_scenario_dir, dest_dir, global_index_dir, keyframe_delta, scenario_postfix)
        if verbose > 1:
            print("Converting scenario {}".format(scenario.name))

        # Read the corresponding damage data entry
        damage_data = DamageData(**ddl[scenario.name]) if (ddl is not None and scenario.name in ddl) else None
        if verbose > 1:
            print("Retrieved damage data {}".format(damage_data))

        # Make sure that the destination scenario directory exists
        dest_scenario_dir = scenario.dest_dir
        if not dest_scenario_dir.exists():
            if not dry_run:
                if verbose > 1:
                    print("The scenario destination directory does not exist yet")
                dest_scenario_dir.mkdir(parents=True)

        if no_conversion is None:
            # Convert all TIFF source files to PNG at the destination
            source_tiffs = scenario.find_source_tiffs()
            await _convert_tiff_to_png(source_tiffs, dest_scenario_dir, verbose, dry_run)
        else:
            # Ensure only the right amount of PNG keyframes is at the
            # destination. Move all other files to the backup directory
            _cleanup_flowheight_pngs(scenario, no_conversion.joinpath(scenario.name), verbose, dry_run)

        # Copy the resulting PNG heightmap to create the normals file
        _create_normals_png(scenario, verbose, dry_run)

        # Extract only the flowheight TIFF source bounds
        source_flowheight_tiffs = scenario.find_source_flowheight_tiffs()
        murgang_bounds = await _extract_image_bounds(source_flowheight_tiffs, verbose, dry_run)

        # Extract the max flowheight bounds
        source_max_flowheight_tiffs = scenario.find_source_max_flowheight_tiffs()
        max_flowheight_bounds = await _extract_image_bounds(source_max_flowheight_tiffs, verbose, dry_run)

        # Generate the manifest
        await _generate_manifest(scenario, murgang_bounds, max_flowheight_bounds, damage_data, verbose, dry_run)

        # Create the fallback scenario, if requested
        await _create_fallback(scenario, murgang_bounds, max_flowheight_bounds, damage_data, verbose, dry_run)


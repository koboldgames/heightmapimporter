# -*- coding: utf-8 -*-

from typing import Iterator, Optional, List
from pathlib import Path
from .murgang_size_de import MurgangSizeDe
from .image_bounds import ImageBounds
from .damage_data import DamageData


class DatasetManifest(object):
    def __init__(self, name: str, houses: List[bool], bridges: List[bool], dams: List[bool], size: MurgangSizeDe, image_frequency: float, murgang_bounds: ImageBounds, maxflowheight_bounds: ImageBounds, damage_data: Optional[DamageData], max_flowheight_file: Path, max_pressure_file: Path, max_velocity_file: Path, flowheights: Iterator[Path], normals: Iterator[Path]) -> None:
        self.name = name
        self.houses = houses
        self.bridges = bridges
        self.dams = dams
        self.size = size

        if damage_data is not None:
            self.person_damage = damage_data.person_damage
            self.property_damage = damage_data.property_damage
            self.supply_damage = damage_data.supply_damage
        else:
            if size == MurgangSizeDe.Gross:
                self.person_damage = 100000.0
                self.property_damage = 5000000.0
                self.supply_damage = 1000000.0
            elif size == MurgangSizeDe.Mittel:
                self.person_damage = 10000.0
                self.property_damage = 1000000.0
                self.supply_damage = 100000.0
            else:
                self.person_damage = 1000.0
                self.property_damage = 10000.0
                self.supply_damage = 50000.0

        self.terrain_bounds = ImageBounds(1359.34, 1950.38)
        self.murgang_bounds = murgang_bounds
        self.maxflowheight_bounds = maxflowheight_bounds
        self.image_frequency = image_frequency
        self.max_flowheight_file = str(max_flowheight_file.name)
        self.max_pressure_file = str(max_pressure_file.name)
        self.max_velocity_file = str(max_velocity_file.name)
        self.flowheights = list(sorted((str(p.name) for p in flowheights)))
        self.normals = list(sorted((str(p.name) for p in normals)))


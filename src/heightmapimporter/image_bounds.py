# -*- coding: utf-8 -*-


class ImageBounds(object):
    def __init__(self, min_val: float, max_val: float) -> None:
        self.min = min_val
        self.max = max_val

